
#ifndef __MENU_H
#define __MENU_H

#include "main.h"
#include "stdbool.h"
#include "stdlib.h"

#include "u8g2.h"
#include "oled_driver.h"

extern const uint8_t icon_pic[][200];
extern const uint8_t LOGO[];
extern uint8_t icon_width[];
#define   EC11_CLK()   HAL_GPIO_ReadPin(EC11_CLK_GPIO_Port,EC11_CLK_Pin)
#define   EC11_DT()   HAL_GPIO_ReadPin(EC11_DT_GPIO_Port,EC11_DT_Pin)
#define KEY_UP() HAL_GPIO_ReadPin(KEY_UP_GPIO_Port, KEY_UP_Pin)
#define KEY_DOWN() HAL_GPIO_ReadPin(KEY_DOWN_GPIO_Port, KEY_DOWN_Pin)
#define KEY_CENTER() HAL_GPIO_ReadPin(KEY_CENTER_GPIO_Port, KEY_CENTER_Pin)
void key_scan();
void ui_proc();
void Menu_Init();

#endif